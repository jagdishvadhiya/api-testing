<?php 

/**
 * summary
 */
class Table extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
         parent::__construct();
    	$this->load->model( "api/Table_model" );
    }
    /**
		* it will show all Table data
		* paramiter -> table_id( optional )
		* - if Pass the any if then it will return table data by table id
		* 
    	*/
    public function index( $table_id = "")
    {
    	
      $response = array();
    	$data=$this->Table_model->table_fetch( $table_id );
    	if( $data != false )
      {
        $response = array(
          "status"     => true,
          "massage"    => "Table Data ".$table_id,
          "data"       =>  $data
        );
      }else {
        $response = array(
          "status"     => false,
          "massage"    => "No Table Data Found"
        );
      }
    	echo json_encode( $response );
    	
    }

    // table action add new or update
    public function tableAction()
    {

  		$response = array();

  		if( $this->input->post() )
  		{
  			
  			$config = array(
  				array(
  					'field' => 'table_name',
  					'label' => 'Table Name',
  					'rules' => 'required'
  				),
  				
  				array(
  					'field' => 'restaurant_id',
  					'label' => 'Restaurant Id',
  					'rules' => 'required'
  				)
  			);
  			$this->form_validation->set_rules( $config );
			  $this->form_validation->set_error_delimiters('', ''); 
  			if( $this->form_validation->run() === true )
  			{
  				$post_data = array(
  					'tm_name' => $this->input->post( 'table_name' ),
  					'tm_rd_id' => $this->input->post( 'restaurant_id' ),
  					'inserted_date' => date( "Y-m-d H:i:s" )
  				);
  				if( $this->input->post( "table_id" ) )
	  			{
	  				if( $this->Table_model->tableExists( $this->input->post( "table_id" ) ) )
	  				{
	  					$post_data['tm_id'] = $this->input->post( 'table_id' );
	  					if( $this->Table_model->tableAction( $post_data ) )
		  				{
		  					$response = array(
				  				'status'  => true,
				  				'message' => 'Table Updated Successfully ...'
			  				);
		  				}else{
		  					$response = array(
				  				'status'  => false,
				  				'message' => 'Table Update Failed !!!'
			  				);
		  				}
	  				}else{
	  					$response = array(
				  				'status'  => false,
				  				'message' => 'Table Not Found !!!'
			  				);
	  				}
	  			}else{
	  				if( $this->Table_model->tableAction( $post_data ) )
	  				{
	  					$response = array(
			  				'status'  => true,
			  				'message' => 'Table Added Successfully ...'
		  				);
	  				}else{
	  					$response = array(
			  				'status'  => false,
			  				'message' => 'Table Add Failed !!!'
		  				);
	  				}
	  			}
  			}else{
  				$response = array(
	  				'status'  => false,
	  				'message' => "All Fields Required !!!",
	  				'form_error' => validation_errors('','')
  				);
  			}
  			
  		}else{
  			$response = array(
  				'status'  => false,
  				'message' => 'Form submit Required !!!'
  			);
  		}

  		echo json_encode( $response );
    }

    //delete table
    	public function tableDelete()
  	{
  		$response = array();
  		
  		if(  $this->input->post( 'table_id' )  )
  		{
  			$table_id = $this->input->post( 'table_id' );
  			if( $this->Table_model->tableExists( $table_id ) )
  			{
  					if( $this->Table_model->deleteTable( $table_id ) )
		  			{
		  				$response = array(
				  			'status' => true,
				  			'massage' => 'Table Deleted Successfully...'
		  				);
		  			}
  			}else{
  				$response = array(
		  			'status' => false,
		  			'massage' => 'Table Not Found !!!'
  				);

  			}
  		}else{
  			$response = array(
	  			'status' => false,
	  			'massage' => 'Please enter Table id !!!'
  			);
  		}
  		echo json_encode( $response );
  	}

}

 ?>