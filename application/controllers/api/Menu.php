<?php 

/**
 * summary
 */
class Menu extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
         parent::__construct();
    	$this->load->model( "api/Menu_model" );
    }

    public function index( $menu_id = "")
    {
      $response = array();
    	$data=$this->Menu_model->menu_fetch( $menu_id );
      if( $data != false )
      {
        $response = array(
          "status"     => "ok",
          "massage"    => "Menu Data ".$menu_id,
          "data"       =>  $data
        );
      }else {
        $response = array(
          "status"     => false,
          "massage"    => "No Menu Data Found"
        );
      }
    	
    	echo json_encode( $response );
    	
    }

    // table action add new or update
    public function menuAction()
    {

  		$response = array();

  		if( $this->input->post() )
  		{
  			$config = array(
  				array(
  					'field' => 'menu_name',
  					'label' => 'Menu Name',
  					'rules' => 'required'
  				),
  				array(
  					'field' => 'menu_price',
  					'label' => 'Menu Price',
  					'rules' => 'required'
  				),
  				array(
  					'field' => 'restaurant_id',
  					'label' => 'Restaurant Id',
  					'rules' => 'required'
  				)
  			);
  			$this->form_validation->set_rules( $config );
  			$this->form_validation->set_error_delimiters('', ''); 
  			if( $this->form_validation->run() === true )
  			{
  				$post_data = array(
  					'menu_name' => $this->input->post( 'menu_name' ),
  					'menu_price' => $this->input->post( 'menu_price' ),
  					'menu_rd_id' => $this->input->post( 'restaurant_id' ),
  					'menu_inserted' => date( "Y-m-d H:i:s" )
  				);
  				if( $this->input->post( "menu_id" ) )
	  			{
	  				if( $this->Menu_model->menuExists( $this->input->post( "menu_id" ) ) )
	  				{
	  					$post_data['menu_id'] = $this->input->post( 'menu_id' );
	  					// $this->User_model->userAction( $post_data );
	  					if( $this->Menu_model->menuAction( $post_data ) )
		  				{
		  					$response = array(
				  				'status'  => 'ok',
				  				'message' => 'Menu Updated Successfully ...'
			  				);
		  				}else{
		  					$response = array(
				  				'status'  => 'error',
				  				'message' => 'Menu Update Failed !!!'
			  				);
		  				}
	  				}else{
	  					$response = array(
				  				'status'  => 'error',
				  				'message' => 'Menu Not Found !!!'
			  				);
	  				}
	  			}else{
	  				if( $this->Menu_model->menuAction( $post_data ) )
	  				{
	  					$response = array(
			  				'status'  => true,
			  				'message' => 'Menu Added Successfully ...'
		  				);
	  				}else{
	  					$response = array(
			  				'status'  => false,
			  				'message' => 'Menu Add Failed !!!'
		  				);
	  				}
	  			}
  			}else{
  				$response = array(
	  				'status'  => false,
	  				'message' => "All Fields Required !!!",
	  				'form_error' => validation_errors('','')
  				);
  			}
  		}else{
  			$response = array(
  				'status'  => false,
  				'message' => 'Form submit Required !!!'
  			);
  		}
  		echo json_encode( $response );
    }

     public function menuDelete()
  	{
  		$response = array();
  		
  		if(  $this->input->post( 'menu_id' )  )
  		{
  			$menu_id = $this->input->post( 'menu_id' );
  			if( $this->Menu_model->menuExists( $menu_id ) )
  			{
  					if( $this->Menu_model->deleteMenu( $menu_id ) )
		  			{
		  				$response = array(
				  			'status' => true,
				  			'massage' => 'Menu Deleted Successfully...'
		  				);
		  			}
  			}else{
  				$response = array(
		  			'status' => false,
		  			'massage' => 'Menu Not Found !!!'
  				);

  			}
  		}else{
  			$response = array(
	  			'status' => false,
	  			'massage' => 'Please enter Menu id !!!'
  			);
  		}
  		echo json_encode( $response );
  	}

}

?>