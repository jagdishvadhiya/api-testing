<?php 

/**
 * summary
 */
class Item extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model( 'api/Item_model' );
    }
    public function index()
    {
    	
    }

    public function itemAction()
    {
    	$response = array();
    	$post_data = file_get_contents('php://input');
    	$post_data = json_decode( $post_data , true );
    	// check data in not empty
    	if( !empty( $post_data ) )
    	{
    		$data = array(
    			'om_date'       => $post_data[ 'order_date' ],
    			'om_table_id'       => $post_data[ 'table_id' ],
    			'om_table_name'       => $post_data[ 'table_name' ],
    			'om_customer_name'       => $post_data[ 'customer_name' ],
    			'om_mobile'       => $post_data[ 'customer_mobile' ],
    			'om_total_amount'       => $post_data[ 'om_total_amount' ],
    			'om_total_item'       => $post_data[ 'order_total_item' ],
    			'om_rd_id'       => $post_data[ 'restaurant_id' ],
    			'om_status'       => $post_data[ 'om_status' ],
    			'om_user_id'       => $post_data[ 'user_id' ],
    			'om_user_name'       => $post_data[ 'user_name' ],
    			'om_inserted_date'       => date( 'Y-m-d H:i:s' ),
    			'order_items'  => $post_data['order_items']
    		);
    		if( isset( $post_data['order_id'] ) )
    		{
    			$data['order_id'] = $post_data['order_id'];
    			
    		}
    		$result = $this->Item_model->itemAction( $data );
    		if( $result != false )
    		{
    			$response = array(
	    			'status'   => true,
	    			'massage'  => 'Item Processed Successfully !!!',
	    			
    			);
    		}else{
    			$response = array(
	    			'status'   => false,
	    			'massage'  => 'Item Action Failed !!!'
	    		);
    		}
    		
    		
    	}else{
    		$response = array(
    			'status'   => false,
    			'massage'  => 'Please enter Data !!!'
    		);
    	}
    	echo json_encode( $response );


    }
}
 ?>