<?php 
/**
 * summary
 */
class restaurant extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
    	$this->load->model( "api/Restaurant_model" );
    }

    public function index()
    {
    	
    	if(isset($_POST))
    	{
    		//checking post field in not empty
    		$config = array(
    			array(
    				'field' => 'restaurant_name',
    				'label' => 'Restaurant Name',
    				'rules' => 'required'
    			),
    			array(
    				'field' => 'restaurant_owner_name',
    				'label' => 'Restaurant Owner Name',
    				'rules' => 'required'
    			),
    			array(
    				'field' => 'restaurant_email',
    				'label' => 'Email',
    				'rules' => 'required'
    			),
    			array(
    				'field' => 'address',
    				'label' => 'Address',
    				'rules' => 'required'
    			)
    		);
    		$this->form_validation->set_rules($config);
    		$this->form_validation->set_error_delimiters('', ''); 
    		if( $this->form_validation->run() === true )
    		{
    		
	    		$post_data = $this->security->xss_clean($_POST);
	    		if($this->Restaurant_model->insert_restuarant($post_data))
	    		{
	    			$response = array(
	    				"status" => true,
	    				"massage" => "Restaurant Successfully Inserted ..."
	    			);
	    		}else{
	    			$response = array(
	    				"status" => false,
	    				"massage" => "Somthing Wrong !!!"
	    			);
	    		}
    		}else{

    			$response = array(
    				"status" => false,
    				"massage" =>  validation_errors()
    			);
    		
    		}

    		
    		echo json_encode($response);
    	}
    }

}
?>