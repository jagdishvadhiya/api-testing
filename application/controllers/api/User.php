<?php 
/**
 * summary
 */
class User extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
    	$this->load->model( "api/User_model" );
    }
    
    	/**
		* it will show all users data
		* paramiter -> id( optional )
		* - if Pass the any id then it will return user data by id
		* 
    	*/
    public function index( $id = "")
    {
      $response = array();
    	$data=$this->User_model->user_fetch( $id );
      if( $data != false )
      {
        $response = array(
          "status"     => true,
          "massage"    => "Users Data ".$id,
          "data"       =>  $data
        );
      }else {
        $response = array(
          "status"     => false,
          "massage"    => "No Users Data Found"
        );
      }
    	
    	echo json_encode( $response );
    	
    }
    
    // login user api method
    public function login()
    {
    	if( $this->input->post() )
  		{
	    	$response = array();
	    	$config = array(
	    		array(
	    				'field' => 'user_email',
	    				'label' => 'Email',
	    				'rules' => 'required'
	    			),
	    		array(
	    				'field' => 'user_password',
	    				'label' => 'Password',
	    				'rules' => 'required'
	    			)
	    	);
	    	$this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('', ''); 
	    	if( $this->form_validation->run() === true )
	    	{
	    		$post_data = array(
	    			'um_email' => $this->input->post('user_email'),
	    			'um_password' => md5( $this->input->post('user_password') )
	    		);
	    		$post_data = $this->security->xss_clean( $post_data );
	    		if( $data = $this->User_model->login( $post_data ) )
	    		// if( $this->User_model->login( $post_data ) )
	    		{
	    			$response = array(
		    			'status'  => true,
		    			'massage' => 'User Login Successfull...',
		    			'data'    => $data
	    			);
	    		}else{
	    			$response = array(
		    			'status'  => false,
		    			'massage' => 'User Details Not Found'
	    			);
	    		}
	    	}else{
	    		$response = array(
	    			'status'  => false,
	    			'massage' => validation_errors()
	    		);
	    	}
	    }else{
	    	$response = array(
	    			'status'  => false,
	    			'massage' => 'Direct Access Dinided !!!'
	    		);
	    }
    	echo json_encode( $response );
    }
  	
  	// add new user
  	public function userAction()
  	{	
  		$response = array();

  		if( $this->input->post() )
  		{
  			$email_rule=( !( $this->input->post('user_id') ) )?"|is_unique[user_master.um_email]":"";
  			
  			$config = array(
  				array(
  					'field' => 'user_name',
  					'label' => 'User Name',
  					'rules' => 'required'
  				),
  				array(
  					'field' => 'user_email',
  					'label' => 'User Email',
  					'rules' => 'required|valid_email'.$email_rule,
  					'errors'=>array('valid_email'=>'Please enter Valid Email!','is_unique'=>'User already Exist!')
  				),
  				array(
  					'field' => 'user_mobile',
  					'label' => 'User Mobile Number',
  					'rules' => 'required'
  				),
  				array(
  					'field' => 'user_password',
  					'label' => 'User Password',
  					'rules' => 'required'
  				),
  				array(
  					'field' => 'user_restaurant_id',
  					'label' => 'Restaurant Id',
  					'rules' => 'required'
  				),
  					array(
  					'field' => 'user_type',
  					'label' => 'User Type',
  					'rules' => 'required'
  				),
  			);
  			$this->form_validation->set_rules( $config );
			  $this->form_validation->set_error_delimiters('', ''); 
  			if( $this->form_validation->run() === true )
  			{
  				$post_data = array(
  					'um_name' => $this->input->post( 'user_name' ),
  					'um_email' => $this->input->post( 'user_email' ),
  					'um_mobile' => $this->input->post( 'user_mobile' ),
  					'um_password' => md5($this->input->post( 'user_password' )),
  					'um_rd_id' => $this->input->post( 'user_restaurant_id' ),
  					'um_type' => $this->input->post( 'user_type' ),
  					'um_inserted_date' => date( "Y-m-d H:i:s" )
  				);
  				if( $this->input->post( "user_id" ) )
	  			{

	  				if( $this->input->post( 'user_id' ) != ""  )
	  				{
	  					$post_data['um_id'] = $this->input->post( 'user_id' );
	  					if( $this->User_model->userAction( $post_data ) )
		  				{
		  					$response = array(
				  				'status'  => true,
				  				'message' => 'User Updated Successfully ...'
			  				);
		  				}else{
		  					$response = array(
				  				'status'  => false,
				  				'message' => 'User Update Failed !!!'
			  				);
		  				}
	  				}else{
	  					$response = array(
			  				'status'  => false,
			  				'message' => 'User Id Needed Blank !!!'
		  				);
	  				}
	  				
	  		
	  			}else{
	  				// $this->User_model->userAction( $post_data );
	  				if( $this->User_model->userAction( $post_data ) )
	  				{
	  					$response = array(
			  				'status'  => true,
			  				'message' => 'User Added Successfully ...'
		  				);
	  				}else{
	  					$response = array(
			  				'status'  => false,
			  				'message' => 'User Add Failed !!!'
		  				);
	  				}
	  				

	  			}
  			}else{
  				$response = array(
	  				'status'  => false,
	  				'message' => "All Fields Required !!!",
	  				'form_error' => validation_errors('','')
  				);
  			}
  			
  		}else{
  			$response = array(
  				'status'  => false,
  				'message' => 'Form submit Required !!!'
  			);
  		}

  		echo json_encode( $response );
  	}
  	// Delete user
  	public function userDelete()
  	{
  		$response = array();
  		
  		if(  $this->input->post( 'user_id' )  )
  		{
  			$user_id = $this->input->post( 'user_id' );
  			if( $this->User_model->userExist( $user_id ) )
  			{
  					if( $this->User_model->deleteUser( $user_id ) )
		  			{
		  				$response = array(
				  			'status' => true,
				  			'massage' => 'User Deleted Successfully...'
		  				);
		  			}
  			}else{
  				$response = array(
		  			'status' => false,
		  			'massage' => 'User Not Found !!!'
  				);

  			}
  			
  		
  		}else{
  			$response = array(
	  			'status' => false,
	  			'massage' => 'Please enter user id !!!'
  			);
  		}
  		echo json_encode( $response );
  	}


}

 ?>