<?php 

/**
 * summary
 */
class Order extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model( 'api/Order_model' );
    }
    /**
     * Show All Orders
     * paramite - order_id(optional)
     * if order_id is pass then get order detalis by order id
     */
    public function index( $order_id = "" )
    {
    	$response = array();
    	if( $order_id == "" )
    	{
    		$result = $this->Order_model->orderDetails();
    		if( $result != false )
    		{
    			$response = array(
    				'status'  => true,
    				'message' => 'All Order Details',
    				'data'    => $result
    			);
    		}else{
    			$response = array(
    				'status'  => false,
    				'message' => 'No Order Details Found'
    			);
    		}
    	}else {
    		$result = $this->Order_model->orderDetails( $order_id );
    		if( $result != false )
    		{
    			$response = array(
    				'status'  => true,
    				'message' => 'Order ID '.$order_id.' Details',
    				'data'    => $result
    			);
    		}else{
    			$response = array(
    				'status'  => false,
    				'message' => 'No Order Details Found'
    			);
    		}
    	}
    	echo json_encode($response);
    }

    /**
     * Order list by status
     * paramite - restaurant_id , order_satatus 
     * paramite Type - Post 
     * it will return order details by restaurant id and order id
     */
    public function orderByStatus()
    {
    	$response = array();
    	if( $this->input->post() )
    	{
    		$config = array(
  				array(
  					'field' => 'restaurant_id',
  					'label' => 'Restaurant Id',
  					'rules' => 'required'
  				),
  				array(
  					'field' => 'order_status',
  					'label' => 'Order Status',
  					'rules' => 'required',
  				)
  			);
  			$this->form_validation->set_rules( $config );
  			$this->form_validation->set_error_delimiters('', ''); 
  			if( $this->form_validation->run() === true )
  			{
  				$post_data = array(
  					'om_rd_id' => $this->input->post( 'restaurant_id' ),
  					'om_status' => $this->input->post( 'order_status' )
  				);
  				$result = $this->Order_model->orderByStatus( $post_data );
  				if( $result != false )
  				{
  					$response = array(
		  				'status'  => true,
		  				'message' => 'Order details by Restaurant Id and status !!!',
		  				'data'    => $result
  					);
  				}else {
  					$response = array(
		  				'status'  => false,
		  				'message' => 'No order details Found !!!',
  					);
  				}

  			}else{
  				$response = array(
	  				'status'  => false,
	  				'message' => 'All Fields Required !!!',
	  				'form_error' => validation_errors('','')
  				);
  			}

    	}else{
    		$response = array(
    			'status'  => false,
    			'message' => 'Please restaurant_id , order_satatus Required'
    		);
    	}
    	echo json_encode($response);
    }

      /**
     * Order list by Date
     * paramite - restaurant_id , order_satatus , start_date , end_date
     * paramite Type - Post 
     * it will return order details by start_date and end_date
     */
    public function orderByDate()
    {
      $response = array();
      if( $this->input->post() )
      {
        $config = array(
          array(
            'field' => 'restaurant_id',
            'label' => 'Restaurant Id',
            'rules' => 'required'
          ),
          array(
            'field' => 'order_status',
            'label' => 'Order Status',
            'rules' => 'required',
          ),
          array(
            'field' => 'start_date',
            'label' => 'Start Date',
            'rules' => 'required|callback_date_valid'
          ),
          array(
            'field' => 'end_date',
            'label' => 'End Date',
            'rules' => 'required|callback_date_valid'
          ),
        );
        $this->form_validation->set_rules( $config );
        $this->form_validation->set_error_delimiters('', ''); 
        if( $this->form_validation->run() === true )
        {
          
          $post_data = array(
            'om_rd_id' => $this->input->post( 'restaurant_id' ),
            'om_status' => $this->input->post( 'order_status' ),
            'om_inserted_date' => $this->input->post( 'start_date' ),
            'om_date' => $this->input->post( 'end_date' ),
          );
          $result = $this->Order_model->orderByDate( $post_data );
          if( $result != false )
          {
            $response = array(
              'status'  => true,
              'message' => 'Order details by Date!!!',
              'data'    => $result
            );
          }else {
            $response = array(
              'status'  => false,
              'message' => 'No order details Found !!!',
            );
          }

        }else{
          
          $response = array(
            'status'  => false,
            'message' => validation_errors('','')
          );
        }

      }else{
        $response = array(
          'status'  => false,
          'message' => 'Please restaurant_id , order_satatus Required, start_date , end_date'
        );
      }
      echo json_encode($response);
    }

    /**
     * Order list by Table
     * paramite - restaurant_id , order_satatus , selected_date , table_id
     * paramite Type - Post 
     * it will return order details by restaurant id , order id , selected_date , table_id
     */
    public function orderByTable()
    {
      $response = array();
      if( $this->input->post() )
      {
        $config = array(
          array(
            'field' => 'restaurant_id',
            'label' => 'Restaurant Id',
            'rules' => 'required'
          ),
          array(
            'field' => 'order_status',
            'label' => 'Order Status',
            'rules' => 'required',
          ),
          array(
            'field' => 'selected_date',
            'label' => 'Selected Date',
            'rules' => 'required|callback_date_valid'
          ),
          array(
            'field' => 'table_id',
            'label' => 'Table Id',
            'rules' => 'required',
          ),
        );
        $this->form_validation->set_rules( $config );
        $this->form_validation->set_error_delimiters('', ''); 
        if( $this->form_validation->run() === true )
        {
          $post_data = array(
            'om_rd_id'         => $this->input->post( 'restaurant_id' ),
            'om_status'        => $this->input->post( 'order_status' ),
            'om_inserted_date' => $this->input->post( 'selected_date' ),
            // 'om_date'          => $this->input->post( 'selected_date' ),
            'om_table_id'      => $this->input->post( 'table_id' )
          );
          $result = $this->Order_model->orderByTable( $post_data );
          if( $result != false )
          {
            $response = array(
              'status'  => true,
              'message' => 'Order details by Table',
              'data'    => $result
            );
          }else {
            $response = array(
              'status'  => false,
              'message' => 'No order details Found !!!',
            );
          }

        }else{
          $response = array(
            'status'  => false,
            'message' => validation_errors('','')
            // 'form_error' => validation_errors('','')
          );
        }

      }else{
        $response = array(
          'status'  => false,
          'message' => 'restaurant_id , order_satatus Required , selected_date , table_id'
        );
      }
      echo json_encode($response);
    }


        /**
     * Order Delete by Order Id
     * paramite - order_id
     * paramite Type - Post 
     * it will Delete Order By Order Id
     */
    public function orderDelete()
    {
      $response = array();
      if( $this->input->post() )
      {
        $config = array(
          array(
            'field' => 'order_id',
            'label' => 'Restaurant Id',
            'rules' => 'required'
          )
        );
        $this->form_validation->set_rules( $config );
        $this->form_validation->set_error_delimiters('', ''); 
        if( $this->form_validation->run() === true )
        {
          
          if( $this->Order_model->orderExists( $this->input->post( 'order_id' ) ) )
          {
              $result = $this->Order_model->deleteOrder( $this->input->post( 'order_id' ) );
              if( $result != false )
              {
                $response = array(
                  'status'  => true,
                  'message' => 'Order Deleted Successfully...'
                );
              }else {
                $response = array(
                  'status'  => false,
                  'message' => 'No order Data Found !!!',
                );
              } 
          }else{
               $response = array(
                  'status'  => false,
                  'message' => 'No order Data Found !!!',
                );
          }

        }else{
          $response = array(
            'status'  => false,
            'message' => validation_errors('','')
          );
        }

      }else{
        $response = array(
          'status'  => false,
          'message' => 'Please Enter Order Id'
        );
      }
      echo json_encode($response);
    }

    /**
     * Order status Change
     * paramite - order_id , order_status 
     * paramite Type - Post 
     * it will Update order details by  order_id , order_status 
     */
    public function orderStatusUpdate()
    {
      $response = array();
      if( $this->input->post() )
      {
        $config = array(
          array(
            'field' => 'order_id',
            'label' => 'Order Id',
            'rules' => 'required'
          ),
          array(
            'field' => 'order_status',
            'label' => 'Order Status',
            'rules' => 'required',
          )
        );
        $this->form_validation->set_rules( $config );
        $this->form_validation->set_error_delimiters('', ''); 
        if( $this->form_validation->run() === true )
        {
          $post_data = array(
            'om_id' => $this->input->post( 'order_id' ),
            'om_status' => $this->input->post( 'order_status' )
          );
          if( $this->Order_model->orderExists( $this->input->post( 'order_id' ) ) )
          {
              $result = $this->Order_model->updateOrder( $post_data );
              if( $result != false )
              {
                $response = array(
                  'status'  => true,
                  'message' => 'Order Updated Successfully...'
                );
              }else {
                $response = array(
                  'status'  => false,
                  'message' => 'No order Data Found !!!',
                );
              } 
          }else{
               $response = array(
                  'status'  => false,
                  'message' => 'No order Data Found !!!',
                );
          }
        
        }else{
          $response = array(
            'status'  => false,
            'form_error' => validation_errors('','')
          );
        }

      }else{
        $response = array(
          'status'  => false,
          'message' => 'Please order_id , order_status '
        );
      }
      echo json_encode($response);
    }

     // home screen api
    // show order count and total
    public function homeScreen()
    {
      // print_r( $this->input->post() );
     
      $response = array();
      if( $this->input->post() )
      {
        $config = array(
          array(
            'field' => 'restaurant_id',
            'label' => 'Restaurant Id',
            'rules' => 'required'
          ),
           array(
            'field' => 'start_date',
            'label' => 'Start Date',
            'rules' => 'required|callback_date_valid'
          )
          
        );
        $this->form_validation->set_rules( $config );
        if( $this->input->post( 'end_date' ) )
        {
          $this->form_validation->set_rules( 'end_date' , 'End Date' , 'required|callback_date_valid' );
        }

        $this->form_validation->set_error_delimiters('', ''); 
        if( $this->form_validation->run() === true )
        {
          
          
            $post_data = array(
              'restaurant_id' => $this->input->post( 'restaurant_id' ),
              'start_date' => $this->input->post( 'start_date' )
              
            );
            if( $this->input->post( 'end_date' ) )
            {
              $post_data['end_date'] = $this->input->post( 'end_date' );
            }
            $result = $this->Order_model->homeScreenOrder( $post_data );
            
            if( $result != false )
            {
              $response = array(
                'status'  => true,
                'message' => 'Order details by Date!!!',
                'data'    => $result
              );
            }else {
              $response = array(
                'status'  => false,
                'message' => 'No order details Found !!!',
              );
            }
        }else{
          $response = array(
            'status'  => false,
            'form_error' => validation_errors('','')
          );
        }

      }else{
        $response = array(
          'status'  => false,
          'message' => 'Please restaurant_id , start_date , end_date '
        );
      }
      echo json_encode($response);
    }
    // date checker callbac function
    public function date_valid( $date )
    {
    	$date_array = explode('-', $date);
    	if( count( $date_array ) == 3 && strlen( $date_array[0] ) == 4 && strlen( $date_array[1] ) == 2 && strlen( $date_array[2] ) == 2 )
    	{
    		if( $date_array[0] <= date( "Y" ) && $date_array[1] >= 01 && $date_array[1] <= 12 && $date_array[2] >= 01 && $date_array[2] <= 31  )
        {
    		  return true;
        }else{
          $this->form_validation->set_message('date_valid','Please Enter Valid {field}');
          return false;
        }
    	}else{
    		$this->form_validation->set_message('date_valid','Please Enter Valid {field}');
    		return false;
    	}
    }


   
    
}
 ?>