<?php 

/**
 * summary
 */
class Menu_model extends CI_Model
{
    /**
     * summary
     */
    public function __construct()
    {
         parent::__construct();
    }

    public function menu_fetch( $menu_id = "" )
    {
        if( $menu_id != "" )
        {
            if( $this->menuExists( $menu_id ) )
            {
                $this->db->where( "menu_id" , $menu_id );
            }else{
                return false;
            }
        }else{
            $this->db->select( "*" );
        }
        $data = $this->db->get('menu_master');
        if( $data->num_rows() > 0 )
        {
            return $data->result_array();
        }else{
            return false;
        }

    }

    // table add and update
    public function menuAction( $data_array )
    {
    	if( isset($data_array['menu_id']) )
        {
            $p_data=$data_array;
            unset($p_data['menu_id']);
            unset($p_data['menu_inserted']);
            $this->db->where('menu_id',$data_array['menu_id']);
            
            if( $this->db->update('menu_master',$p_data) )
            {
               return true;
            }else{

               return false;
            } 
        }else{
            if( $this->db->insert( 'menu_master' , $data_array ) )
            {
               return true;
            }else{

               return false;
            }
        }
    }

    // delete Table by id
    public function deleteMenu( $menu_id )
    {
        if( $menu_id != "" )
        {
            if( $this->db->delete('menu_master', array('menu_id' => $menu_id)) )
            {
                return true;
            }else{
                return false;
            }
        }
    }

       // check menu exists or not
    public function menuExists( $menu_id )
    {
    	$menu_data = $this->db->select( "*" )->where( "menu_id" , $menu_id )->get( "menu_master" );
        if( $menu_data->num_rows() > 0 )
        {
           return true;
        }else{
            return false;
        }
    }
}

?>