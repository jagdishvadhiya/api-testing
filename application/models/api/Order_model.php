<?php 

/**
 * summary
 */
class Order_model extends CI_Model
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();

    }
     /**
     * return all order details 
     *  parameter order_id ( optional )
     *  if pass order_id then it will return order id details
     */
    public function orderDetails( $order_id = "" )
    {
    	if( $order_id != "" )
    	{
    		$this->db->where( "om_id" , $order_id );
    	}
    	$this->db->select( "*" );
    	$data = $this->db->get( "order_master" );
    	if( $data->num_rows() > 0 )
    	{
    		return $data->result_array();	
    	}else{
    		return false;
    	}
    }

    //return Order list by restaurant id and order status
    public function orderByStatus( $data_array )
    {
    	if( !empty( $data_array ) )
    	{
    		$data = $this->db->where( $data_array )->get( "order_master" );
    		if( $data->num_rows() > 0 )
    		{
    			return $data->result_array();
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }

     //return Order list by restaurant id , order status ,satrt date , end date
    public function orderByDate( $data_array )
    {
        if( !empty( $data_array ) )
        {
            $like_condition = array(
                'om_inserted_date' => $data_array[ "om_inserted_date" ],
                'om_date'          => $data_array[ "om_date" ]
            );
            $this->db->select('*');
            $this->db->from('order_master');
            $this->db->like( $like_condition );
            $data = $this->db->get();
            if( $data->num_rows() > 0 )
            {
                return $data->result_array();
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
      //return Order list by restaurant id , order status ,selected_date date , table_id
    public function orderByTable( $data_array )
    {
        if( !empty( $data_array ) )
        {
            $like_condition = array(
                'om_inserted_date' => $data_array[ "om_inserted_date" ]
                // 'om_date'          => $data_array[ "om_date" ]
            );
            $where_condition = $data_array;
            unset( $where_condition["om_inserted_date"] );
            // unset( $where_condition["om_date"] );
           
            $this->db->select('*');
            $this->db->where($where_condition);
            $this->db->from('order_master');
            $this->db->like( $like_condition );            
            $data = $this->db->get();
            if( $data->num_rows() > 0 )
            {
                return $data->result_array();
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    // delete Order by id
    public function deleteOrder( $order_id )
    {
        if( $order_id != "" )
        {
      
            if( $this->db->delete('order_master', array('om_id' => $order_id)) )
            {
                return true;
            }else{
                return false;
            }
        }else {
            return false;
        }
    }

     // delete Order by id
    public function updateOrder( $data_array )
    {
        if( $data_array['om_id'] != "" )
        {
      
            $this->db->where('om_id',$data_array['om_id']);
            
            if( $this->db->update('order_master',$data_array) )
            {
               return true;
            }else{

               return false;
            }
        }else {
            return false;
        }
    }    

    //return Home Screen Order list by restaurant id ,satrt date , end date
    public function homeScreenOrder( $data_array )
    {
        
        if( !empty( $data_array ) )
        {
            $this->db->select( "om_total_amount" );
            if( isset( $data_array['end_date'] ) )
            {
               $this->db->where('om_date >=', $data_array[ "start_date" ]);
               $this->db->where('om_date <=', $data_array[ "end_date" ]);
            }else {
                 $this->db->where('om_date', $data_array[ "start_date" ]);
            }
            $this->db->from('order_master');
            
            $data = $this->db->get();
            if( $data->num_rows() > 0 )
            {
                $total_order = $data->num_rows();
                $total_order_amount = 0;
                $ndata = $data->result_array();
                foreach ($ndata as $value) {
                    $total_order_amount += $value["om_total_amount"];
                }
                $return_data = array(
                    'total_order'        => $total_order,
                    'total_order_amount' => $total_order_amount
                );
                return $return_data;
            }else {
                return false;
            }

        }else{
            return false;
        }
    }

    // check Order exists or not
    public function orderExists( $order_id )
    {
        $order_data = $this->db->select( "*" )->where( "om_id" , $order_id )->get( "order_master" );
        if( $order_data->num_rows() > 0 )
        {
           return true;
        }else{
            return false;
        }
    }
}

 ?>