<?php 

/**
 * summary
 */
class Item_model extends CI_Model
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
    }


    // add and update order and item details
    public function itemAction( $dataArray )
    {
    	if( !empty( $dataArray ) )
    	{
    		$row_data = $dataArray;
    		unset( $row_data['order_items'] );
    		
    		// check for insert or update items and order
    		if( isset( $dataArray[ 'order_id' ] ) )
    		{
    			//update items and orders
    			$order_id = $dataArray[ 'order_id' ];
    			if( $this->orderExist( $order_id ) ){}else{ return false; }
    			unset( $row_data['order_id'] );
    			$result = $this->db->where( 'om_id', $order_id )->update( "order_master" , $row_data );
    			if( $result )
    			{
    				$this->deleteItems( $order_id );
    				$item_data = array();
    				foreach ($dataArray[ 'order_items' ] as $key => $value) 
				     {
			    	      	$i_data = array(
			    					'im_name'             =>  $value[ 'item_name' ],
			    					'im_item_id'          =>  $value[ 'item_id' ],
			    					'im_qty'              =>  $value[ 'item_qty' ],
			    					'im_amout'            =>  $value[ 'amount' ],
			    					'im_total_amout'      =>  $value[ 'total_amount' ],
			    					'im_order_id'         =>  $order_id
			    					
			    					);
			    	      	array_push($item_data, $i_data);
				     }
				    
    				if( $this->db->insert_batch('item_master', $item_data) )
		    		 {
		    		 	return true;
		    		 }else {
		    		 	
    					return false;
		    		 }
    			}else {
    				return false;
    			}
    		}else{
    			// insert data in order table
    			$result = $this->db->insert( "order_master" , $row_data );
    			if( $result )
    			{
    				$order_id = $this->db->insert_id();
    				$item_data = array();
				   	 foreach ($dataArray[ 'order_items' ] as $key => $value) 
				     {
			    	      	$item_data[] = array(
			    					'im_name'             =>  $value[ 'item_name' ],
			    					'im_item_id'          =>  $value[ 'item_id' ],
			    					'im_qty'              =>  $value[ 'item_qty' ],
			    					'im_amout'            =>  $value[ 'amount' ],
			    					'im_total_amout'      =>  $value[ 'total_amount' ],
			    					'im_order_id'         =>  $order_id,
			    					'im_inserted_date'    =>  date( 'Y-m-d H:i:s' )
			    					);
				     }
		    		 if( $this->db->insert_batch('item_master', $item_data) )
		    		 {
		    		 	return true;
		    		 }else{
		    		 	return false;
		    		 }
	    		
    			}else{
    				return false;
    			}
    		}
    		exit();

    	}else{
    		return false;
    	}
    }

    // check order exist
    public function orderExist( $order_id )
    {
    	if ( $order_id != "" )
    	{
    		$result = $this->db->select( "*" )->where( "om_id" , $order_id )->get( "order_master" );
    		if(	$result->num_rows() > 0 )
    		{
    			return true;
    		}else{
    			return false;
    		}
	    }else{
	    	return false;
	    }
    }
    // get item id
    public function deleteItems( $order_id )
    {
    	 $this->db->where( "im_order_id" , $order_id )->delete( "item_master" );
    						
    }
}
 ?>