<?php 

/**
 * summary
 */
class Restaurant_model extends CI_Model
{
    /**
     * summary
     */
    public function __construct()
    {
         parent::__construct();
    }
    public function insert_restuarant($post_data)
    {
    	if(!empty($post_data))
    	{
    		$rest_data = array(
	    			'rd_restaurant_name'       => $post_data["restaurant_name"],
	    			'rd_restaurant_owner_name' => $post_data["restaurant_owner_name"],
	    			'rd_restaurant_email'      => $post_data["restaurant_email"],
	    			'rd_mobile_number'         => $post_data["mobile_number"],
	    			'rd_address'               => $post_data["address"],
	    			'rd_inserted_date'         => date("Y-m-d H:i:m")
    			
    			);

    		if($this->db->insert("restaurant_detail",$rest_data))
    		{
    			$restaurant_id = $this->db->insert_id();
    			$user_data = array(
    				'um_name' => $post_data['restaurant_owner_name'],
    				'um_email' => $post_data['restaurant_email'],
    				'um_mobile' => $post_data['mobile_number'],
    				'um_password' => md5($post_data['password']),
    				'um_rd_id' => $restaurant_id,
    				'um_type' => "admin",
    				'um_inserted_date' => date("Y-m-d H:i:m"),
    			);

	    		if($this->db->insert("user_master",$user_data))
	    		{
	    			return true;
	    		}else{
	    			return false;
	    		}   			
    		}else{
    			return false;
    		}
    	}
    }
}
?>