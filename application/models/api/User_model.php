<?php 

/**
 * summary
 */
class User_model extends CI_Model
{
    /**
     * summary
     */
    public function __construct()
    {
         parent::__construct();
    }
    /**
     * Fetch userdata 
     * paramiter - id(optional)
     */
    public function user_fetch( $id = "" )
    {
    	if( $id != "" )
    	{
            if( $this->userExist( $id ) )
            {
                $this->db->where( "um_id" , $id );
            }else{
                return false;
            }
    		
    	}else{
            $this->db->select( "*" );
            
        }
        $data = $this->db->get('user_master');
        if( $data->num_rows() > 0 )
        {
            return $data->result_array();
        }else{
            return false;
        }
    	
    }

    // user login method check credientials
    public function login( $data_array )
    {
        if( !empty( $data_array ) )
        {

            $this->db->select( "*" );
            $this->db->where($data_array);
            $query = $this->db->get( "user_master" );
            if( $query->num_rows() > 0 )
            {
                // return true;
                return $query->result_array();
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    // add New User And Update User Method
    public function userAction( $data_array )
    {
        if( isset($data_array['um_id']) )
        {
            $p_data=$data_array;
            unset($p_data['um_id']);
            unset($p_data['um_inserted_date']);
            // print_r($p_data);
            
            $this->db->where('um_id',$data_array['um_id']);
            
            if( $this->db->update('user_master',$p_data) )
            {
               return true;
            }else{

               return false;
            } 
        }else{
            // echo "no user id";
            // exit;
            if( $this->db->insert( 'user_master' , $data_array ) )
            {
               return true;
            }else{

               return false;
            }
        }
        

    }

    // delete user by id
    public function deleteUser( $user_id )
    {
        if( $user_id != "" )
        {
      
            if( $this->db->delete('user_master', array('um_id' => $user_id)) )
            {
                return true;
            }else{
                return false;
            }
        }
    }

    // check user exist or not by user id
    public function userExist( $user_id )
    {
        $user_data = $this->db->select( "*" )->where( "um_id" , $user_id )->get( "user_master" );
        if( $user_data->num_rows() > 0 )
        {
           return true;
        }else{
            return false;
        }
    }
}

 ?>