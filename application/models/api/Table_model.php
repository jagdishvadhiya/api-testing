<?php 

/**
 * summary
 */
class Table_model extends CI_Model
{
    /**
     * summary
     */
    public function __construct()
    {
         parent::__construct();
    }

    /**
     * Fetch tabledata 
     * paramiter - table_id(optional)
     */
    public function table_fetch( $table_id = "" )
    {
        if( $table_id != "" )
        {
            if( $this->tableExists( $table_id ) )
            {
                $this->db->where( "tm_id" , $table_id );
            }else{
                return false;
            }
            
        }else{
            $this->db->select( "*" );
            
        }
        $data = $this->db->get('table_master');
        if( $data->num_rows() > 0 )
        {
            return $data->result_array();
        }else{
            return false;
        }

    	 
    }

    // table add and update
    public function tableAction( $data_array )
    {
    	if( isset($data_array['tm_id']) )
        {
            $p_data=$data_array;
            unset($p_data['tm_id']);
            unset($p_data['inserted_date']);
            // print_r($p_data);
            
            $this->db->where('tm_id',$data_array['tm_id']);
            
            if( $this->db->update('table_master',$p_data) )
            {
               return true;
            }else{

               return false;
            } 
        }else{
            if( $this->db->insert( 'table_master' , $data_array ) )
            {
               return true;
            }else{

               return false;
            }
        }
    }

    // delete Table by id
    public function deleteTable( $table_id )
    {
        if( $table_id != "" )
        {
      
            if( $this->db->delete('table_master', array('tm_id' => $table_id)) )
            {
                return true;
            }else{
                return false;
            }
        }
    }

    // check table exists or not
    public function tableExists( $table_id )
    {
    	$table_data = $this->db->select( "*" )->where( "tm_id" , $table_id )->get( "table_master" );
        if( $table_data->num_rows() > 0 )
        {
           return true;
        }else{
            return false;
        }
    }
}

?>